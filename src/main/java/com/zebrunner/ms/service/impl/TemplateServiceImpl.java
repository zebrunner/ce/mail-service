package com.zebrunner.ms.service.impl;

import com.zebrunner.common.s3.AwsS3Properties;
import com.zebrunner.ms.service.TemplateService;
import com.zebrunner.ms.service.exception.ServiceException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.S3Exception;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class TemplateServiceImpl implements TemplateService {

    private final S3Client s3Client;
    private final AwsS3Properties awsS3Properties;

    private String templatesDirectory;

    @Value("${spring.mail.templates-directory}")
    public void setTemplatesDirectory(String templatesDirectory) {
        this.templatesDirectory = templatesDirectory.endsWith("/") ? templatesDirectory : templatesDirectory + "/";
    }

    @Override
    public String loadAndProcess(String templateName, Object templateModel) {
        String key = templatesDirectory + templateName;
        try {
            InputStream content = s3Client.getObject(
                    rb -> rb.bucket(awsS3Properties.getBucket()).key(key).build(),
                    ResponseTransformer.toInputStream()
            );
            return processIntoHtml(content, templateModel);
        } catch (S3Exception e) {
            throw new ServiceException("Unable to load template by key '{" + key + "}'", e);
        }
    }

    private String processIntoHtml(InputStream content, Object model) {
        try {
            Template template = new Template(
                    UUID.randomUUID().toString(),
                    new InputStreamReader(content),
                    new Configuration(Configuration.VERSION_2_3_30)
            );
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        } catch (IOException | TemplateException e) {
            throw new ServiceException("Error processing template", e);
        }
    }

}
