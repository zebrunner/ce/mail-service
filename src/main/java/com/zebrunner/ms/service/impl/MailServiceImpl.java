package com.zebrunner.ms.service.impl;

import com.zebrunner.ms.domain.Attachment;
import com.zebrunner.ms.domain.MailData;
import com.zebrunner.ms.service.MailService;
import com.zebrunner.ms.service.TemplateService;
import com.zebrunner.ms.service.exception.ServiceException;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.ContentDisposition;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private static final String ERR_MSG_MAIL_MESSAGE_MALFORMED = "Message should contain plain text only, or template name and template model, or all three";

    private final JavaMailSenderImpl mailSender;
    private final TemplateService templateService;

    @Setter(onMethod = @__(@Value("${spring.mail.sender.name}")))
    private String senderUsername;

    @Setter(onMethod = @__(@Value("${spring.mail.sender.email}")))
    private String senderEmail;

    @Override
    public void processAndSend(MailData mailData) {
        validateMandatoryAttributes(mailData);
        Map<String, Object> templateModel = new HashMap<>();

        if (mailData.getTemplateModel() != null)
            templateModel = mailData.getTemplateModel();

        if (mailData.getAttachments() != null)
            templateModel.put("attachments", mailData.getAttachments());

        if (!ObjectUtils.isEmpty(mailData.getTemplateName())) {
            String htmlText = templateService.loadAndProcess(mailData.getTemplateName(), templateModel);
            mailData.setHtmlText(htmlText);
        }

        mailSender.send(buildPreparator(mailData));
    }

    private void validateMandatoryAttributes(MailData mailData) {
        boolean valid = mailData != null
                && !ObjectUtils.isEmpty(mailData.getSubject())
                && !CollectionUtils.isEmpty(mailData.getToEmails())
                && ((!ObjectUtils.isEmpty(mailData.getTemplateName()))
                || !ObjectUtils.isEmpty(mailData.getPlainText()));
        if (!valid) {
            throw new ServiceException(ERR_MSG_MAIL_MESSAGE_MALFORMED);
        }
    }

    private MimeMessagePreparator buildPreparator(MailData mailData) {
        return mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setSubject(mailData.getSubject());
            messageHelper.setTo(filterEmails(mailData.getToEmails()).toArray(String[]::new));

            String htmlText = mailData.getHtmlText();
            if (htmlText != null) {
                messageHelper.setText(htmlText, true);
            }

            String plainText = mailData.getPlainText();
            if (plainText != null) {
                messageHelper.setText(plainText, false);
            }

            Set<String> ccEmails = mailData.getCcEmails();
            if (ccEmails != null) {
                messageHelper.setCc(filterEmails(ccEmails).toArray(String[]::new));
            }

            Set<String> bccEmails = mailData.getBccEmails();
            if (bccEmails != null) {
                messageHelper.setBcc(filterEmails(bccEmails).toArray(String[]::new));
            }

            List<Attachment> attachments = mailData.getAttachments();
            if (attachments != null) {
                addAttachments(messageHelper, attachments);
            }
            addSenderInfo(messageHelper);
        };
    }

    private Set<String> filterEmails(Set<String> emails) {
        return emails.stream()
                     .filter(StringUtils::hasText)
                     .collect(Collectors.toSet());
    }

    private void addAttachments(MimeMessageHelper messageHelper, List<Attachment> attachments) {
        attachments.stream()
                   .filter(Objects::nonNull)
                   .forEach(attachment -> attachToMessage(messageHelper, attachment));
    }

    @SneakyThrows
    private void attachToMessage(MimeMessageHelper messageHelper, Attachment attachment) {
        String attachmentName = attachment.getName();
        String contentId = attachmentName.replaceAll("[^A-Za-z0-9]", "_");

        Resource resource = new ByteArrayResource(attachment.getData()) {
            @Override
            public String getFilename() {
                return attachmentName;
            }
        };

        messageHelper.addInline(contentId, resource);

        BodyPart bodyPart = messageHelper.getMimeMultipart().getBodyPart(String.format("<%s>", contentId));

        // Removes any old dispositions ('Content-Disposition' headers)
        bodyPart.setDisposition(null);

        // Creates a new disposition with file name and inserts created disposition into message header
        ContentDisposition contentDisposition = new ContentDisposition("inline");
        contentDisposition.setParameter("filename", attachmentName);
        bodyPart.setDisposition(contentDisposition.toString());
    }

    private void addSenderInfo(MimeMessageHelper msg) throws UnsupportedEncodingException, MessagingException {
        msg.setFrom(senderEmail, senderUsername);
    }

}
