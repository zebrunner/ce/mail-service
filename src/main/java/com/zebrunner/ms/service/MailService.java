package com.zebrunner.ms.service;

import com.zebrunner.ms.domain.MailData;

public interface MailService {

    /**
     * Processes supplied mail data into full-fledged mail message and sends it
     * @param mailData aggregated mail data describing mail content and recipients
     */
    void processAndSend(MailData mailData);

}
