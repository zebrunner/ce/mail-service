package com.zebrunner.ms.service;

public interface TemplateService {

    /**
     * Loads HTML template by name from underlying storage and processes it with the given template model
     *
     * @param templateName template name to be used for processing
     * @param templateModel the model object, typically a Map that contains model names as keys and model objects
     *                      as values
     * @return processing result HTML as {@link String}
     */
    String loadAndProcess(String templateName, Object templateModel);

}
