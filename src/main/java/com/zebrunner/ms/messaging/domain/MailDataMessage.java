package com.zebrunner.ms.messaging.domain;

import com.zebrunner.ms.domain.Attachment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Object aggregating mail message data, that will be used to construct mail message to be sent.
 * Message can contain plain text only, or template name and template model, or all three (in such cases multipart
 * message will be sent containing both plain text and html).
 */
@Getter
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
public final class MailDataMessage {

    @ToString.Include
    private Set<String> toEmails;
    private Set<String> ccEmails;
    private Set<String> bccEmails;
    @ToString.Include
    private String subject;
    private String plainText;
    private String templateName;
    private Map<String, Object> templateModel;
    private List<Attachment> attachments;

}
