package com.zebrunner.ms.messaging.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Queue {

    public static final String SEND_EMAIL = "email.send.ms";

}
