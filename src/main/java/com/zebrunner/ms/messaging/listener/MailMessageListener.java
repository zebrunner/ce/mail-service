package com.zebrunner.ms.messaging.listener;

import com.zebrunner.ms.domain.MailData;
import com.zebrunner.ms.messaging.config.Queue;
import com.zebrunner.ms.messaging.domain.MailDataMessage;
import com.zebrunner.ms.service.MailService;
import com.zebrunner.ms.service.exception.ServiceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class MailMessageListener {

    private final MailService mailService;

    @RabbitListener(queues = Queue.SEND_EMAIL)
    public void processMailMessage(@Payload MailDataMessage message) {
        try {
            MailData mailData = convert(message);
            mailService.processAndSend(mailData);
        } catch (ServiceException e) {
            log.error("Failed to process message " + message, e);
        } catch (Exception e) {
            log.error("Unexpected error: failed to process message " + message, e);
        }
    }

    private MailData convert(MailDataMessage message) {
        return MailData.builder()
                       .subject(message.getSubject())
                       .toEmails(message.getToEmails())
                       .ccEmails(message.getCcEmails())
                       .bccEmails(message.getBccEmails())
                       .plainText(message.getPlainText())
                       .templateName(message.getTemplateName())
                       .templateModel(message.getTemplateModel())
                       .attachments(message.getAttachments())
                       .build();
    }

}
