package com.zebrunner.ms.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Message can contain plain text only, or template name and template model,
 * or all three (in such cases multipart message will be sent containing both plain text and html).
 */
@Getter
@Setter
@Builder
public class MailData {

    private Set<String> toEmails;
    private Set<String> ccEmails;
    private Set<String> bccEmails;
    private String subject;

    /**
     * Plain text part of the body
     */
    private String plainText;

    /**
     * HTML template name to be used to construct HTML part of the body
     */
    private String templateName;

    /**
     * HTML model object, that contains model names as keys and model objects as values
     */
    private Map<String, Object> templateModel;

    /**
     * HTML text that is result of processing HTML template and its model
     */
    private String htmlText;

    /**
     * Email attachments
     */
    private List<Attachment> attachments;

}
