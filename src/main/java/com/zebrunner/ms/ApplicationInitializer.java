package com.zebrunner.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationInitializer {

    //Set environment variable spring.profiles.active=dev for local development
    public static void main(String[] args) {
        SpringApplication.run(ApplicationInitializer.class, args);
    }

}
