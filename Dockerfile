FROM adoptopenjdk:11-jre-hotspot
COPY build/libs/mail-service.jar /app/mail-service.jar
CMD ["java", "-jar", "/app/mail-service.jar"]
EXPOSE 8080
