# Mail service

Mail service used to send emails from Zebrunner services.

# Checking out and building

To check out the project and build from source, do the following:

    git clone git://github.com/zebrunner/mail-service.git
    cd mail-service
    ./gradlew build
